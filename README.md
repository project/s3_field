# S3 Field
Reference AWS S3 bucket objects (public or private) through Drupal fields without replacing the entire public and/or private filesystem ([drupal/s3fs](https://www.drupal.org/project/s3fs) exists for that!). Configured through field settings.

## What can you do?
- Create a field that references an S3 object key.
- Create media that uses this field as a source.
- Display the field as a **plain text URL or link** to a Drupal route, which redirects to an S3 URL (pre-signed if the object is private).

## Dependencies
- [drupal/key](https://www.drupal.org/project/key) (Used for storing AWS credentials to access S3 objects.)
- PHP >= 7.1

## Limitations
- Available field widget is a textfield to enter the key for an existing object in your S3 bucket, file upload is not supported.
- Available field formatters are plain text URL or link.
- S3 Region & Bucket are configured in field settings, so the same field can't reference objects across different buckets.
- The URL to access the file will look something like `/s3f/node/123/field_file` and redirect to S3. It will not link directly to S3 to avoid breaking page caching with pre-signed URLs.

## Install & Configure
- Require module with composer (`composer require drupal/s3_field`)
- Enable module (`key` module will also be enabled if it isn't already)
- Create 2 keys in Drupal with your AWS credentials: 1 access key and 1 secret access key (**Important:** make sure the keys' user has read access to the S3 bucket! Creating a user with read access only for the bucket(s) you'll use is recommended.)
- Create an S3 field on your content type (or create a new media type using an S3 field as its source)
- Configure your new S3 field with region, bucket name, and credentials to access your S3 bucket
- Adjust your content/media type's form/display as desired
- Create new content/media!

---

## FUTURE GOALS
- Use a stream wrapper to serve S3 objects from your Drupal domain, using the key as a file path.
- Create/reference file entities for S3 objects.
- Limit and validate file types for field.
- Image field type.
- Autocomplete form input or browser to select S3 object key from existing objects in bucket.
- Upload file form input to upload files to your S3 bucket from the Drupal form.
