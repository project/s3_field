<?php

namespace Drupal\s3_field\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Aws\S3\S3Client;

/**
 * S3 file field media source.
 *
 * @MediaSource(
 *   id = "s3_file",
 *   label = @Translation("S3 File"),
 *   description = @Translation("Use AWS S3 file object for reusable media."),
 *   allowed_field_types = {"s3_file"},
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class S3File extends MediaSourceBase {

  /**
   * Key for "Name" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_NAME = 'name';

  /**
   * Key for "MIME type" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_MIME = 'mimetype';

  /**
   * Key for "File size" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_SIZE = 'filesize';

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
      static::METADATA_ATTRIBUTE_MIME => $this->t('MIME type'),
      static::METADATA_ATTRIBUTE_SIZE => $this->t('File size'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $key = $media->get($this->configuration['source_field'])->getValue()[0]['value'];
    // If the source field is not required, it may be empty.
    if (!$key) {
      return parent::getMetadata($media, $attribute_name);
    }

    $settings = $this->getSourceFieldSettings($media);
    switch ($attribute_name) {
      case static::METADATA_ATTRIBUTE_NAME:
      case 'default_name':
        $parts = explode('/', $key);
        return array_pop($parts);

      case static::METADATA_ATTRIBUTE_MIME:
        return $this->getMimeType($settings, $key);

      case static::METADATA_ATTRIBUTE_SIZE:
        return $this->getSize($settings, $key);

      case 'thumbnail_uri':
        return $this->getThumbnail($settings, $key) ?: parent::getMetadata($media, $attribute_name);

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * Returns the MIME type of the object.
   *
   * @return string
   *   The MIME type of the object, e.g. image/jpeg or text/xml.
   */
  protected function getMimeType($settings, $key) {
    $metadata = $this->getObjectMetadata($settings, $key);
    return $metadata['ContentType'] ?? '';
  }

  /**
   * Returns the size of the object.
   *
   * @return string
   *   The size of the object in bytes.
   */
  protected function getSize($settings, $key) {
    $metadata = $this->getObjectMetadata($settings, $key);
    return $metadata['ContentLength'] ?? 0;
  }

  /**
   * Gets the thumbnail image URI based on the object.
   *
   * @return string
   *   File URI of the thumbnail image or NULL if there is no specific icon.
   */
  protected function getThumbnail($settings, $key) {
    $icon_base = $this->configFactory->get('media.settings')->get('icon_base_uri');

    // We try to automatically use the most specific icon present in the
    // $icon_base directory, based on the MIME type. For instance, if an
    // icon file named "pdf.png" is present, it will be used if the file
    // matches this MIME type.
    $mimetype = $this->getMimeType($settings, $key);
    if (!empty($mimetype)) {
      $mimetype = explode('/', $mimetype);
      $icon_names = [
        $mimetype[0] . '--' . $mimetype[1],
        $mimetype[1],
        $mimetype[0],
      ];
      foreach ($icon_names as $icon_name) {
        $thumbnail = $icon_base . '/' . $icon_name . '.png';
        if (is_file($thumbnail)) {
          return $thumbnail;
        }
      }
    }

    return NULL;
  }

  /**
   * Returns settings for the source field.
   *
   * @return array
   */
  protected function getSourceFieldSettings(MediaInterface $media) {
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('media', $media->bundle());
    return $field_definitions[$this->configuration['source_field']]->getSettings();
  }

  /**
   * Returns metadata for the S3 object.
   *
   * @return \Aws\Result|array
   */
  protected function getObjectMetadata($settings, $key) {
    $client = new S3Client([
      'version' => 'latest',
      'region' => $settings['region'],
      'bucket' => $settings['bucket'],
      'credentials' => [
        'key' => \Drupal::service('key.repository')->getKey($settings['access_key'])->getKeyValue(),
        'secret' => \Drupal::service('key.repository')->getKey($settings['secret_key'])->getKeyValue(),
      ],
    ]);
    try {
      return $client->headObject([
        'Bucket' => $settings['bucket'],
        'Key' => $key,
      ]);
    } catch (\Exception $e) {
      \Drupal::logger('s3_field')->error($e->getMessage(), $e->getTrace());
      return [];
    }
  }

}
