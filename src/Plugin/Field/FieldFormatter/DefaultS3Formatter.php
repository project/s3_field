<?php

namespace Drupal\s3_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 's3_default' formatter.
 *
 * @FieldFormatter(
 *   id = "s3_default",
 *   label = @Translation("Link"),
 *   field_types = {
 *     "s3_file"
 *   }
 * )
 */
class DefaultS3Formatter extends S3FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'text' => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['text'] = [
      '#type' => 'textfield',
      '#title' => t('Link Text'),
      '#description' => t('Defaults to filename from key.'),
      '#default_value' => $this->getSetting('text'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $text = $this->getSetting('text');
    if ($text) {
      $summary[] = $this->t('Link text: "@text"', ['@text' => $text]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $url = $this->getUrl($items->getEntity(), $delta);
      $text = $this->getSetting('text');
      $key_parts = explode('/', $item->value);
      $filename = array_pop($key_parts);
      $elements[$delta] = [
        '#type' => 'link',
        '#title' => $text ?: $filename,
        '#url' => $url,
      ];
    }

    return $elements;
  }

}
