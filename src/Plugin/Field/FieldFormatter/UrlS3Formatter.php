<?php

namespace Drupal\s3_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 's3_url' formatter.
 *
 * @FieldFormatter(
 *   id = "s3_url",
 *   label = @Translation("Plain text URL"),
 *   field_types = {
 *     "s3_file"
 *   }
 * )
 */
class UrlS3Formatter extends S3FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $absolute = $this->getSetting('absolute');
    foreach ($items as $delta => $item) {
      $url = $this->getUrl($items->getEntity(), $delta);
      $elements[$delta] = [
        '#plain_text' => $url->toString(),
      ];
    }

    return $elements;
  }

}
