<?php

namespace Drupal\s3_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;


/**
 * Base class for S3 field formatter plugin implementations.
 *
 * @ingroup field_formatter
 */
abstract class S3FormatterBase extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'absolute' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['absolute'] = [
      '#type' => 'checkbox',
      '#title' => t('Absolute URL'),
      '#default_value' => $this->getSetting('absolute'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $absolute = $this->getSetting('absolute');
    $summary[] = $absolute
      ? $this->t('Absolute URL')
      : $this->t('Relative URL');

    return $summary;
  }

  /**
   * Get Drupal URL for field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param int $delta
   *   The field value's delta.
   *
   * @return \Drupal\Core\Url
   *   Drupal URL which will redirect to S3.
   */
  public function getUrl(EntityInterface $entity, int $delta = 0) {
    return Url::fromRoute('s3_field.file',
      [
        'entity_type' => $entity->getEntityTypeId(),
        'entity' => $entity->id(),
        'field_name' => $this->fieldDefinition->getName(),
        'delta' => $delta,
      ],
      [
        'absolute' => $this->getSetting('absolute'),
      ]
    );
  }

}
