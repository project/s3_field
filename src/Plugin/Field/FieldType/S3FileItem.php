<?php

namespace Drupal\s3_field\Plugin\Field\FieldType;

/**
 * Defines the 's3_file' field type.
 *
 * @FieldType(
 *   id = "s3_file",
 *   label = @Translation("S3 File"),
 *   description = @Translation("This field stores an AWS S3 object key."),
 *   default_widget = "s3_textfield",
 *   default_formatter = "s3_default"
 * )
 */
class S3FileItem extends S3ItemBase {

}
