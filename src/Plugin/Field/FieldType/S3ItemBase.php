<?php

namespace Drupal\s3_field\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for s3 field type plugin implementations.
 */
abstract class S3ItemBase extends FieldItemBase {

  const KEY_MAX_LENGTH = 1024;

  const REGIONS = [
    'us-east-1',
    'us-east-2',
    'us-west-1',
    'us-west-2',
    'us-gov-west-1',
    'eu-west-1',
    'eu-west-2',
    'eu-west-3',
    'eu-central-1',
    'ap-south-1',
    'ap-southeast-1',
    'ap-southeast-2',
    'ap-northeast-1',
    'ap-northeast-2',
    'ap-northeast-3',
    'sa-east-1',
    'cn-north-1',
    'cn-northwest-1',
    'ca-central-1',
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'region' => 'us-east-1',
      'bucket' => '',
      'access_key' => NULL,
      'secret_key' => NULL,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Key'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element = [];
    $settings = $this->getSettings();

    $region_options = [];
    foreach (self::REGIONS as $region) {
      $region_options[$region] = $region;
    }
    $element['region'] = [
      '#type' => 'select',
      '#title' => 'Bucket Region',
      '#required' => TRUE,
      '#empty_option' => t('Select Region'),
      '#default_value' => $settings['region'],
      '#options' => $region_options,
    ];

    $element['bucket'] = [
      '#type' => 'textfield',
      '#title' => t('Bucket Name'),
      '#required' => TRUE,
      '#default_value' => $settings['bucket'],
    ];

    $element['access_key'] = [
      '#type' => 'key_select',
      '#title' => t('Access Key'),
      '#required' => TRUE,
      '#default_value' => $settings['access_key'],
    ];

    $element['secret_key'] = [
      '#type' => 'key_select',
      '#title' => t('Secret Key'),
      '#required' => TRUE,
      '#default_value' => $settings['secret_key'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => self::KEY_MAX_LENGTH,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
        'Length' => [
          'max' => self::KEY_MAX_LENGTH,
          'maxMessage' => t('%name: the key can not be longer than @max characters.', ['%name' => $this->getFieldDefinition()->getLabel(), '@max' => self::KEY_MAX_LENGTH]),
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['value'] = $random->name() . '.pdf';
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '';
  }

}
