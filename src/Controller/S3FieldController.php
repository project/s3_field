<?php

namespace Drupal\s3_field\Controller;

use Aws\S3\S3Client;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\key\KeyRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Class S3FieldController
 *
 * @package Drupal\s3_field\Controller
 */
class S3FieldController extends ControllerBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * S3FieldController constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\key\KeyRepository $keyRepository
   *   The key repository.
   */
  public function __construct(EntityFieldManager $entityFieldManager, KeyRepository $keyRepository) {
    $this->entityFieldManager = $entityFieldManager;
    $this->keyRepository = $keyRepository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('key.repository')
    );
  }

  /**
   * Access check for the entity & field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param int $delta
   *   The field value's delta.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function fieldAccess(ContentEntityInterface $entity, string $field_name, int $delta) {
    // Check if entity and field exists.
    $field_definitions = $this->entityFieldManager
      ->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    $definition = $field_definitions[$field_name] ?? NULL;
    if (!$definition) {
      throw new NotFoundHttpException();
    }

    // Check if field has a value.
    $items = $entity->get($field_name);
    $values = $items->getValue();
    if (empty($values[$delta])) {
      throw new NotFoundHttpException();
    }

    $account = $this->currentUser();
    $entity_access = $entity->access('view', $account);
    $field_access = $items->access('view', $account);
    return AccessResult::allowedIf($entity_access && $field_access);
  }

  /**
   * Redirect to S3 based on key in entity's field.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param int $delta
   *   The field value's delta.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the requested file does not exist.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   */
  public function download(Request $request, ContentEntityInterface $entity, string $field_name, int $delta) {
    $field_definitions = $this->entityFieldManager
      ->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    $definition = $field_definitions[$field_name];

    $key = $entity->get($field_name)->getValue()[$delta]['value'];

    $settings = $definition->getSettings();
    // Create S3 client from field settings to get object URL.
    $client = new S3Client([
      'version' => 'latest',
      'region' => $settings['region'],
      'bucket' => $settings['bucket'],
      'credentials' => [
        'key' => $this->keyRepository->getKey($settings['access_key'])->getKeyValue(),
        'secret' => $this->keyRepository->getKey($settings['secret_key'])->getKeyValue(),
      ],
    ]);

    $exists = $client->doesObjectExist($settings['bucket'], $key);
    if (!$exists) {
      throw new NotFoundHttpException();
    }

    // Check if object is public (ACL contains "AllUsers").
    $acl = $client->getObjectAcl([
      'Bucket' => $settings['bucket'],
      'Key' => $key,
    ]);
    $grantees = array_column($acl['Grants'], 'Grantee');
    $public = count(array_filter($grantees, function ($grantee) {
      return isset($grantee['URI']) && strpos($grantee['URI'], 'AllUsers') !== FALSE;
    })) > 0;

    $timeout = 60;
    if (!$public) {
      $redirect = (string) $client->createPresignedRequest(
        $client->getCommand('GetObject', [
          'Bucket' => $settings['bucket'],
          'Key' => $key,
        ]),
        "+$timeout seconds"
      )->getUri();
    } else {
      $redirect = $client->getObjectUrl($settings['bucket'], $key);
    }

    $response = new TrustedRedirectResponse($redirect);
    if (!$public) {
      $response->getCacheableMetadata()->setCacheMaxAge($timeout - 30);
    }
    return $response;
  }

}
